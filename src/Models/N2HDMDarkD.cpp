#include "ScannerS/Models/N2HDMDarkD.hpp"

#include "AnyHdecay.hpp"
#include "ScannerS/Constants.hpp"
#include "ScannerS/Constraints/STU.hpp"
#include "ScannerS/Interfaces/HiggsBoundsSignals.hpp"
#include "ScannerS/Tools/SushiTables.hpp"
#include "ScannerS/Utilities.hpp"
#include <Eigen/Core>
#include <cmath>
#include <cstddef>
#include <map>
#include <sstream>
#include <utility>

// -------------------------------- Generation --------------------------------
namespace {
double CalcSortedAlpha(double mHa, double mHb, double alphaIn) {
  using ScannerS::Constants::pi;
  if (mHa >= mHb)
    return alphaIn;
  else {
    if (sin(alphaIn) > 0)
      return alphaIn - pi / 2;
    else
      return alphaIn + pi / 2;
  }
}

// get the ordering matching Eq. (16) in 1805.00966
auto DarkDoubletMixmat(double alpha) {
  Eigen::Vector3i trans;
  trans << 0, 2, 1;
  auto result = ScannerS::Utilities::OrderedMixMat3d(alpha, 0, 0, {0, 1, 2});
  result *= Eigen::PermutationMatrix<3>{trans};
  return result;
}

std::array<double, 8> CalcLambdas(const std::array<double, 2> &mHi, double mHD,
                                  double mAD, double mHDp,
                                  const Eigen::Matrix3d &R, double vs, double v,
                                  double L2, double L8, double m22sq) {
  const double v2 = pow(v, 2);
  const double vs2 = pow(vs, 2);

  const double L1 =
      1 / v2 * (pow(mHi[0] * R(0, 0), 2) + pow(mHi[1] * R(1, 0), 2));
  const double L3 = 1 / v2 * (2 * (mHDp * mHDp - m22sq) - vs2 * L8);
  const double L4 = 1 / v2 * (mAD * mAD + mHD * mHD - 2 * mHDp * mHDp);
  const double L5 = 1 / v2 * (mHD * mHD - mAD * mAD);
  const double L6 =
      1 / vs2 * (pow(mHi[0] * R(0, 2), 2) + pow(mHi[1] * R(1, 2), 2));
  const double L7 =
      1 / v / vs *
      (pow(mHi[0], 2) * R(0, 0) * R(0, 2) + pow(mHi[1], 2) * R(1, 0) * R(1, 2));
  return {L1, L2, L3, L4, L5, L6, L7, L8};
}

double CalcM11sq(double vs, double v, const std::array<double, 8> &L) {
  return -1 / 2. * (v * v * L[0] + vs * vs * L[6]);
}

double CalcMssq(double vs, double v, const std::array<double, 8> &L) {
  return -1 / 2. * (v * v * L[6] + vs * vs * L[5]);
}

} // namespace

namespace ScannerS::Models {

N2HDMDarkD::ParameterPoint::ParameterPoint(const AngleInput &in)
    : mHi{Utilities::Sorted(std::array<double, 2>({in.mHa, in.mHb}))},
      mHD{in.mHD}, mAD{in.mAD}, mHDp{in.mHDp}, alpha{CalcSortedAlpha(
                                                   in.mHb, in.mHa, in.alpha)},
      R{DarkDoubletMixmat(alpha)}, vs{in.vs}, v{in.v},
      L{CalcLambdas(mHi, mHD, mAD, mHDp, R, vs, v, in.L2, in.L8, in.m22sq)},
      m11sq{CalcM11sq(vs, v, L)}, m22sq{in.m22sq}, mssq{CalcMssq(vs, v, L)} {}

// -------------------------------- Pheno --------------------------------
void N2HDMDarkD::RunHdecay(ParameterPoint &p) {
  using namespace AnyHdecay;
  static const Hdecay hdec{};
  auto result = hdec.n2hdmDarkDoublet(
      DarkAMass{p.mAD}, DarkHcMass{p.mHDp}, DarkHMass{p.mHD}, HMass{p.mHi[0]},
      HMass{p.mHi[1]}, MixAngle{p.alpha}, Lambda{p.L[7]},
      SquaredMassPar{p.m22sq}, SingletVev{p.vs});
  for (auto [key, value] : result)
    p.data.Store(std::string(key), value);
}

Interfaces::HiggsBoundsSignals::HBInput<N2HDMDarkD::nHzero, N2HDMDarkD::nHplus>
N2HDMDarkD::HiggsBoundsInput(
    ParameterPoint &p,
    const Interfaces::HiggsBoundsSignals::HiggsBoundsSignals<nHzero, nHplus>
        &hbhs) {

  Interfaces::HiggsBoundsSignals::HBInput<nHzero, nHplus> hb;
  for (size_t i = 0; i != nHzeroVisible; ++i) {
    hb.CP_value(i) = 1;
    hb.Mh(i) = p.mHi[i];
  }

  for (size_t i = 0; i != nHzeroVisible; ++i) {
    hb.GammaTotal_hj(i) = p.data["w_"s + namesHzero[i]];
    // HiggsBounds_neutral_input_SMBR
    hb.BR_hjss(i) = p.data["b_"s + namesHzero[i] + "_ss"];
    hb.BR_hjcc(i) = p.data["b_"s + namesHzero[i] + "_cc"];
    hb.BR_hjbb(i) = p.data["b_"s + namesHzero[i] + "_bb"];
    hb.BR_hjtt(i) = p.data["b_"s + namesHzero[i] + "_tt"];
    hb.BR_hjmumu(i) = p.data["b_"s + namesHzero[i] + "_mumu"];
    hb.BR_hjtautau(i) = p.data["b_"s + namesHzero[i] + "_tautau"];
    hb.BR_hjZga(i) = p.data["b_"s + namesHzero[i] + "_Zgam"];
    hb.BR_hjgaga(i) = p.data["b_"s + namesHzero[i] + "_gamgam"];
    hb.BR_hjgg(i) = p.data["b_"s + namesHzero[i] + "_gg"];
    hb.BR_hjWW(i) = p.data["b_"s + namesHzero[i] + "_WW"];
    hb.BR_hjZZ(i) = p.data["b_"s + namesHzero[i] + "_ZZ"];
    hb.BR_hjinvisible(i) = p.data["b_"s + namesHzero[i] + "_HDHD"] +
                           p.data["b_"s + namesHzero[i] + "_ADAD"] +
                           p.data["b_"s + namesHzero[i] + "_HDpHDm"];
  }
  // HiggsBounds_neutral_input_nonSMBR
  hb.BR_hkhjhi(1, 0, 0) = p.data["b_H2_H1H1"];
  for (size_t i = 0; i != nHzeroVisible; ++i) {
    const double cVV = p.data["c_"s + namesHzero[i] + "VV"];
    const double cu = p.data["c_"s + namesHzero[i] + "uu_e"];
    const double cd = p.data["c_"s + namesHzero[i] + "dd_e"];
    const double cl = p.data["c_"s + namesHzero[i] + "ll_e"];
    // HiggsBounds_neutral_input_LEP
    hb.XS_ee_hjZ_ratio(i) = pow(cVV, 2);
    hb.XS_ee_bbhj_ratio(i) = pow(cd, 2);
    hb.XS_ee_tautauhj_ratio(i) = pow(cl, 2);
    // HiggsBounds_neutral_input_hadr
    //   Tevatron
    double gg = cxnH0_.GGe(p.mHi[i], cu, cd, Tools::SushiTables::Collider::TEV);
    double bb = cxnH0_.BBe(p.mHi[i], cd, Tools::SushiTables::Collider::TEV);
    double gg0 = cxnH0_.GGe(p.mHi[i], 1, 1, Tools::SushiTables::Collider::TEV);
    double bb0 = cxnH0_.BBe(p.mHi[i], 1, Tools::SushiTables::Collider::TEV);
    hb.TEV_CS_hj_ratio(i) = (gg + bb) / (gg0 + bb0);
    hb.TEV_CS_gg_hj_ratio(i) = gg / gg0;
    hb.TEV_CS_bb_hj_ratio(i) = bb / bb0;
    hb.TEV_CS_hjW_ratio(i) = pow(cVV, 2);
    hb.TEV_CS_hjZ_ratio(i) = pow(cVV, 2);
    hb.TEV_CS_vbf_ratio(i) = pow(cVV, 2);
    hb.TEV_CS_tthj_ratio(i) = pow(cu, 2);      //< neglects ZH>ttH diagrams
    hb.TEV_CS_thj_tchan_ratio(i) = pow(cu, 2); //< LO correct
    hb.TEV_CS_thj_schan_ratio(i) = pow(cu, 2); //< LO correct
    //   LHC7
    gg = cxnH0_.GGe(p.mHi[i], cu, cd, Tools::SushiTables::Collider::LHC7);
    bb = cxnH0_.BBe(p.mHi[i], cd, Tools::SushiTables::Collider::LHC7);
    gg0 = cxnH0_.GGe(p.mHi[i], 1, 1, Tools::SushiTables::Collider::LHC7);
    bb0 = cxnH0_.BBe(p.mHi[i], 1, Tools::SushiTables::Collider::LHC7);
    hb.LHC7_CS_hj_ratio(i) = (gg + bb) / (gg0 + bb0);
    hb.LHC7_CS_gg_hj_ratio(i) = gg / gg0;
    hb.LHC7_CS_bb_hj_ratio(i) = bb / bb0;
    hb.LHC7_CS_hjW_ratio(i) = pow(cVV, 2);
    hb.LHC7_CS_hjZ_ratio(i) = pow(cVV, 2);
    hb.LHC7_CS_vbf_ratio(i) = pow(cVV, 2);
    hb.LHC7_CS_tthj_ratio(i) = pow(cu, 2);      //< neglects ZH>ttH diagrams
    hb.LHC7_CS_thj_tchan_ratio(i) = pow(cu, 2); //< LO correct
    hb.LHC7_CS_thj_schan_ratio(i) = pow(cu, 2); //< LO correct
    //   LHC8
    gg = cxnH0_.GGe(p.mHi[i], cu, cd, Tools::SushiTables::Collider::LHC8);
    bb = cxnH0_.BBe(p.mHi[i], cd, Tools::SushiTables::Collider::LHC8);
    gg0 = cxnH0_.GGe(p.mHi[i], 1, 1, Tools::SushiTables::Collider::LHC8);
    bb0 = cxnH0_.BBe(p.mHi[i], 1, Tools::SushiTables::Collider::LHC8);
    hb.LHC8_CS_hj_ratio(i) = (gg + bb) / (gg0 + bb0);
    hb.LHC8_CS_gg_hj_ratio(i) = gg / gg0;
    hb.LHC8_CS_bb_hj_ratio(i) = bb / bb0;
    hb.LHC8_CS_hjW_ratio(i) = pow(cVV, 2);
    hb.LHC8_CS_hjZ_ratio(i) = pow(cVV, 2);
    hb.LHC8_CS_vbf_ratio(i) = pow(cVV, 2);
    hb.LHC8_CS_tthj_ratio(i) = pow(cu, 2);      //< neglects ZH>ttH diagrams
    hb.LHC8_CS_thj_tchan_ratio(i) = pow(cu, 2); //< LO correct
    hb.LHC8_CS_thj_schan_ratio(i) = pow(cu, 2); //< LO correct
    //   LHC13
    gg = cxnH0_.GGe(p.mHi[i], cu, cd, Tools::SushiTables::Collider::LHC13);
    bb = cxnH0_.BBe(p.mHi[i], cd, Tools::SushiTables::Collider::LHC13);
    gg0 = cxnH0_.GGe(p.mHi[i], 1, 1, Tools::SushiTables::Collider::LHC13);
    bb0 = cxnH0_.BBe(p.mHi[i], 1, Tools::SushiTables::Collider::LHC13);
    hb.LHC13_CS_hj_ratio(i) = (gg + bb) / (gg0 + bb0);
    hb.LHC13_CS_gg_hj_ratio(i) = gg / gg0;
    hb.LHC13_CS_bb_hj_ratio(i) = bb / bb0;
    const auto x_VH = hbhs.GetVHCxns(p.mHi[i], cVV, cu, cd);
    const auto x_VH_ref = hbhs.GetVHCxns(p.mHi[i], 1, 1, 1);
    hb.LHC13_CS_hjZ_ratio(i) = x_VH.x_hZ / x_VH_ref.x_hZ;
    hb.LHC13_CS_gg_hjZ_ratio(i) = x_VH.x_gg_hZ / x_VH_ref.x_gg_hZ;
    hb.LHC13_CS_qq_hjZ_ratio(i) = x_VH.x_qq_hZ / x_VH_ref.x_qq_hZ;
    hb.LHC13_CS_hjW_ratio(i) = x_VH.x_hW / x_VH_ref.x_hW;
    hb.LHC13_CS_vbf_ratio(i) = pow(cVV, 2);
    hb.LHC13_CS_tthj_ratio(i) = pow(cu, 2);      //< neglects ZH>ttH diagrams
    hb.LHC13_CS_thj_tchan_ratio(i) = pow(cu, 2); //< LO correct
    hb.LHC13_CS_thj_schan_ratio(i) = pow(cu, 2); //< LO correct
    hb.LHC13_CS_tWhj_ratio(i) =
        Interfaces::HiggsBoundsSignals::tWHratio(cVV, cu);
  }

  // dark Higgs masses
  hb.Mh(2) = p.mHD;
  hb.Mh(3) = p.mAD;
  hb.Mhplus(0) = p.mHDp;

  return hb;
}

Constraints::STUDetail::STUParameters
N2HDMDarkD::STUInput(const ParameterPoint &p) {
  return N2HDM::STUInput(p.mAD, {p.mHi[0], p.mHi[1], p.mHD}, p.mHDp, 0, p.R);
}

bool N2HDMDarkD::EWPValid(const ParameterPoint &p) {
  return (p.mHDp + p.mHD > Constants::mW) && (p.mHDp + p.mAD > Constants::mW) &&
         (p.mHD + p.mAD > Constants::mZ) && (2 * p.mHDp > Constants::mZ);
}

// ------------------------------------ DM ------------------------------------
std::map<std::string, double>
Models::N2HDMDarkD::MOInput(const ParameterPoint &p) {
  return {{"ms2", p.mssq},
          {"M11s", p.m11sq},
          {"M22s", p.m22sq},
          {"Lam1", p.L[0]},
          {"Lam2", p.L[1]},
          {"Lam3", p.L[2]},
          {"Lam4", p.L[3]},
          {"Lam5", p.L[4]},
          {"Lam6", p.L[5]},
          {"Lam7", p.L[6]},
          {"Lam8", p.L[7]},
          {"vs", p.vs},
          {"MZ", Constants::mZ},
          {"Gf", Constants::Gf},
          {"aS", Constants::alphaSAtMz},
          {"alfSMZ", Constants::alphaSAtMz},
          {"aEWinv", 1 / Constants::alphaAtMz},
          {"Mu2", Constants::mC},
          {"Md3", Constants::mB},
          {"Mu3", Constants::mT},
          {"Me2", Constants::mMu},
          {"Me3", Constants::mTau}};
}

// -------------------------------- Properties --------------------------------

const Tools::SushiTables N2HDMDarkD::cxnH0_;

std::string N2HDMDarkD::ParameterPoint::ToString() const {
  auto os = std::ostringstream{};
  auto printer = Utilities::TSVPrinter(os);
  for (double x : mHi)
    printer << x;
  printer << mHD << mAD << mHDp;
  printer << alpha;
  printer << R.format(Utilities::TSVPrinter::matrixFormat);
  printer << vs << v;
  for (double l : L)
    printer << l;
  printer << m11sq << m22sq << mssq;
  for (const auto &[key, value] : data)
    printer << value;
  return os.str();
}

void N2HDMDarkD::CalcCouplings(ParameterPoint &p) {
  for (size_t i = 0; i != nHzeroVisible; ++i) {
    p.data.Store("c_"s + namesHzero[i] + "VV", p.R(i, 0));
    p.data.Store("c_"s + namesHzero[i] + "uu_e", p.R(i, 0));
    p.data.Store("c_"s + namesHzero[i] + "dd_e", p.R(i, 0));
    p.data.Store("c_"s + namesHzero[i] + "ll_e", p.R(i, 0));
  }
}

void N2HDMDarkD::CalcCXNs(ParameterPoint &p) {
  for (size_t i = 0; i != nHzeroVisible; ++i) {
    p.data.Store("x_"s + namesHzero[i] + "_ggH",
                 cxnH0_.GGe(p.mHi.at(i), p.data["c_"s + namesHzero[i] + "uu_e"],
                            p.data["c_"s + namesHzero[i] + "dd_e"],
                            Tools::SushiTables::Collider::LHC13));
    p.data.Store("x_"s + namesHzero[i] + "_bbH",
                 cxnH0_.BBe(p.mHi.at(i), p.data["c_"s + namesHzero[i] + "dd_e"],
                            Tools::SushiTables::Collider::LHC13));
  }
}

std::vector<double> N2HDMDarkD::ParamsEVADE(const ParameterPoint &p) {
  return {0,      p.v,    p.vs,   p.L[0], p.L[1],  p.L[2],  p.L[3], p.L[4],
          p.L[5], p.L[6], p.L[7], 0,      p.m11sq, p.m22sq, p.mssq};
}

} // namespace ScannerS::Models
