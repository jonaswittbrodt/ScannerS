#include "ScannerS/Models/TwoHDM.hpp"

#include "ScannerS/Constants.hpp"
#include <algorithm>
#include <cmath>

namespace ScannerS {
namespace Models {
bool TwoHDM::BFB(double L1, double L2, double L3, double L4, double abs_L5) {
  if (L1 < 0 || L2 < 0)
    return false;
  if (std::sqrt(L1 * L2) + L3 < 0)
    return false;
  if (std::sqrt(L1 * L2) + L3 + L4 < abs_L5)
    return false;
  return true;
}

double TwoHDM::MaxUnitarityEV(double L1, double L2, double L3, double L4,
                              double abs_L5) {
  using ScannerS::Constants::pi;
  using std::abs;
  using std::pow;
  using std::sqrt;
  return std::max(
      {abs(3 / 2. * (L1 + L2) +
           sqrt(9 / 4. * pow(L1 - L2, 2) + pow(2 * L3 + L4, 2))),
       abs(3 / 2. * (L1 + L2) -
           sqrt(9 / 4. * pow(L1 - L2, 2) + pow(2 * L3 + L4, 2))),
       abs(1 / 2. * (L1 + L2) + 1 / 2. * sqrt(pow(L1 - L2, 2) + 4 * L4 * L4)),
       abs(1 / 2. * (L1 + L2) - 1 / 2. * sqrt(pow(L1 - L2, 2) + 4 * L4 * L4)),
       abs(1 / 2. * (L1 + L2) +
           1 / 2. * sqrt(pow(L1 - L2, 2) + 4 * abs_L5 * abs_L5)),
       abs(1 / 2. * (L1 + L2) -
           1 / 2. * sqrt(pow(L1 - L2, 2) + 4 * abs_L5 * abs_L5)),
       abs(L3 + 2 * L4 + 3 * abs_L5), abs(L3 + 2 * L4 - 3 * abs_L5),
       abs(L3 + abs_L5), abs(L3 - abs_L5), abs(L3 + L4), abs(L3 - L4)});
}

TwoHDM::HpCoups TwoHDM::TwoHDMHpCoups(double tbeta, TwoHDM::Yuk type) {
  switch (type) {
  case TwoHDM::Yuk::typeI:
  case TwoHDM::Yuk::leptonSpecific:
    return {1 / tbeta, 1 / tbeta};
  case TwoHDM::Yuk::typeII:
  case TwoHDM::Yuk::flipped:
    return {1 / tbeta, tbeta};
  default:
    return {};
  }
}

} // namespace Models
} // namespace ScannerS
