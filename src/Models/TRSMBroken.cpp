#include "ScannerS/Models/TRSMBroken.hpp"

#include "ScannerS/Constraints/STU.hpp"
#include "ScannerS/Interfaces/HiggsBoundsSignals.hpp"
#include "ScannerS/Tools/ScalarWidths.hpp"
#include "ScannerS/Utilities.hpp"
#include <cmath>
#include <sstream>

namespace ScannerS::Models {

// -------------------------------- Generation --------------------------------
namespace {
std::array<double, 6> CalcLambdas(const std::array<double, 3> &mHi,
                                  const Eigen::Matrix3d &R, double v, double vs,
                                  double vx) {
  const double mH1sq = mHi[0] * mHi[0];
  const double mH2sq = mHi[1] * mHi[1];
  const double mH3sq = mHi[2] * mHi[2];
  const double lH = (mH1sq * pow(R(0, 0), 2) + mH2sq * pow(R(1, 0), 2) +
                     mH3sq * pow(R(2, 0), 2)) /
                    (2. * pow(v, 2));
  const double lS = (mH1sq * pow(R(0, 1), 2) + mH2sq * pow(R(1, 1), 2) +
                     mH3sq * pow(R(2, 1), 2)) /
                    (2. * pow(vs, 2));
  const double lX = (mH1sq * pow(R(0, 2), 2) + mH2sq * pow(R(1, 2), 2) +
                     mH3sq * pow(R(2, 2), 2)) /
                    (2. * pow(vx, 2));
  const double lHS = (mH1sq * R(0, 0) * R(0, 1) + mH2sq * R(1, 0) * R(1, 1) +
                      mH3sq * R(2, 0) * R(2, 1)) /
                     (v * vs);
  const double lHX = (mH1sq * R(0, 0) * R(0, 2) + mH2sq * R(1, 0) * R(1, 2) +
                      mH3sq * R(2, 0) * R(2, 2)) /
                     (v * vx);
  const double lSX = (mH1sq * R(0, 1) * R(0, 2) + mH2sq * R(1, 1) * R(1, 2) +
                      mH3sq * R(2, 1) * R(2, 2)) /
                     (vs * vx);
  return {lH, lS, lX, lHS, lHX, lSX};
}

double CalcMuHsq(const std::array<double, 6> &L, double v, double vs,
                 double vx) {
  return (-2 * pow(v, 2) * L[0] - pow(vs, 2) * L[3] - pow(vx, 2) * L[4]) / 2.;
}

double CalcMuSsq(const std::array<double, 6> &L, double v, double vs,
                 double vx) {
  return (-2 * pow(vs, 2) * L[1] - pow(v, 2) * L[3] - pow(vx, 2) * L[5]) / 2.;
}

double CalcMuXsq(const std::array<double, 6> &L, double v, double vs,
                 double vx) {
  return (-2 * pow(vx, 2) * L[2] - pow(v, 2) * L[4] - pow(vs, 2) * L[5]) / 2.;
}

} // namespace

TRSMBroken::ParameterPoint::ParameterPoint(const AngleInput &in)
    : mHi{Utilities::Sorted(std::array<double, 3>({in.mHa, in.mHb, in.mHc}))},
      R{Utilities::OrderedMixMat3d(-in.t1, -in.t2, -in.t3,
                                   {in.mHa, in.mHb, in.mHc})},
      theta{Utilities::Scaled(Utilities::MixMatAngles3d(R), -1)}, v{in.v},
      vs{in.vs}, vx{in.vx}, L{CalcLambdas(mHi, R, v, vs, vx)}, muHsq{CalcMuHsq(
                                                                   L, v, vs,
                                                                   vx)},
      muSsq{CalcMuSsq(L, v, vs, vx)}, muXsq{CalcMuXsq(L, v, vs, vx)} {}

// -------------------------------- Pheno --------------------------------
Constraints::STUDetail::STUParameters
TRSMBroken::STUInput(const ParameterPoint &p) {
  return TRSM::STUInput(p.mHi, {p.R(0, 0), p.R(1, 0), p.R(2, 0)});
}

void TRSMBroken::CalculateBRs(
    ParameterPoint &p,
    const Interfaces::HiggsBoundsSignals::HiggsBoundsSignals<nHzero, nHplus>
        &hbhs) {
  using ScalarWidths::TripleHiggsDecEqual, ScalarWidths::TripleHiggsDec,
      ScalarWidths::SMLikeBRs, ScalarWidths::ScaledSMWidths;
  using std::pow;
  const size_t H1 = 0, H2 = 1, H3 = 2;
  p.data.Merge(TRSM::TripleHCoups(p.L, p.R, p.v, p.vs, p.vx));

  double w_H2_H1H1 =
      TripleHiggsDecEqual(p.mHi[H2], p.mHi[H1], p.data["c_H1H1H2"]);
  double w_H3_H1H1 =
      TripleHiggsDecEqual(p.mHi[H3], p.mHi[H1], p.data["c_H1H1H3"]);
  double w_H3_H2H2 =
      TripleHiggsDecEqual(p.mHi[H3], p.mHi[H2], p.data["c_H2H2H3"]);
  double w_H3_H1H2 =
      TripleHiggsDec(p.mHi[H3], p.mHi[H2], p.mHi[H1], p.data["c_H1H2H3"]);

  p.data.Merge(SMLikeBRs(
      ScaledSMWidths(hbhs.GetSMBRs(p.mHi[H1]), pow(p.R(H1, 0), 2)), 0, "H1"));
  p.data.Merge(
      SMLikeBRs(ScaledSMWidths(hbhs.GetSMBRs(p.mHi[H2]), pow(p.R(H2, 0), 2)),
                w_H2_H1H1, "H2"));
  p.data.Merge(
      SMLikeBRs(ScaledSMWidths(hbhs.GetSMBRs(p.mHi[H3]), pow(p.R(H3, 0), 2)),
                w_H3_H1H1 + w_H3_H2H2 + w_H3_H1H2, "H3"));

  p.data.Store("b_H2_H1H1", w_H2_H1H1 / p.data["w_H2"]);
  p.data.Store("b_H3_H1H1", w_H3_H1H1 / p.data["w_H3"]);
  p.data.Store("b_H3_H2H2", w_H3_H2H2 / p.data["w_H3"]);
  p.data.Store("b_H3_H1H2", w_H3_H1H2 / p.data["w_H3"]);
}

void TRSMBroken::CalculateCXNs(
    ParameterPoint &p,
    const Interfaces::HiggsBoundsSignals::HiggsBoundsSignals<nHzero, nHplus>
        &hbhs) {
  for (size_t i = 0; i != nHzero; ++i) {
    auto cxn = hbhs.GetSMCxns(p.mHi[i]);
    const double k = p.R(i, 0) * p.R(i, 0);
    p.data.Store("x_"s + namesHzero[i] + "W", cxn.x_hW * k);
    p.data.Store("x_"s + namesHzero[i] + "Z", cxn.x_hZ * k);
    p.data.Store("x_"s + namesHzero[i] + "_gg", cxn.x_h_gg * k);
    p.data.Store("x_"s + namesHzero[i] + "_bb", cxn.x_h_bb * k);
    p.data.Store("x_"s + namesHzero[i] + "_vbf", cxn.x_h_vbf * k);
    p.data.Store("x_"s + namesHzero[i] + "tt", cxn.x_tth * k);
  }
}

Interfaces::HiggsBoundsSignals::HBInputEffC<TRSMBroken::nHzero,
                                            TRSMBroken::nHplus>
TRSMBroken::HiggsBoundsInput(
    ParameterPoint &p,
    const Interfaces::HiggsBoundsSignals::HiggsBoundsSignals<nHzero, nHplus>
        &hbhs) {
  CalculateBRs(p, hbhs);
  CalculateCXNs(p, hbhs);
  Interfaces::HiggsBoundsSignals::HBInputEffC<nHzero, nHplus> in;
  for (size_t i = 0; i != nHzero; ++i) {
    in.Mh(i) = p.mHi[i];
  }
  in.SetSMlikeScaled({p.R(0, 0), p.R(1, 0), p.R(2, 0)});

  // HiggsBounds_neutral_input_nonSMBR
  in.BR_hkhjhi(1, 0, 0) = p.data["b_H2_H1H1"];
  in.BR_hkhjhi(2, 0, 0) = p.data["b_H3_H1H1"];
  in.BR_hkhjhi(2, 0, 1) = p.data["b_H3_H1H2"];
  in.BR_hkhjhi(2, 1, 1) = p.data["b_H3_H2H2"];
  in.BR_hkhjhi(2, 1, 0) = in.BR_hkhjhi(2, 0, 1);
  return in;
}

std::string TRSMBroken::ParameterPoint::ToString() const {
  std::ostringstream os;
  auto printer = Utilities::TSVPrinter(os);
  for (double m : mHi)
    printer << m;
  printer << R.format(Utilities::TSVPrinter::matrixFormat);
  for (double a : theta)
    printer << a;
  printer << v << vs << vx;
  for (double l : L)
    printer << l;
  printer << muHsq << muSsq << muXsq;
  for (const auto &[key, value] : data)
    printer << value;
  return os.str();
}

} // namespace ScannerS::Models
