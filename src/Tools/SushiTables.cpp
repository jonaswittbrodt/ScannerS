#include "ScannerS/Tools/SushiTables.hpp"

#include "ScannerS/Utilities.hpp"
#include "ScannerS/config.h"
#include <fstream>
#include <string>
#include <vector>

ScannerS::Tools::SushiTables::SushiTables() {
  const std::string filepath{SCANNERS_DATA_DIR + std::string("/ggH_bbH.dat")};
  std::ifstream data(filepath);
  if (data.fail())
    throw(std::ios_base::failure("Could not open SusHi data file at " +
                                 filepath));

  std::string buffer;
  std::getline(data, buffer);
  auto m = Utilities::ParseToDoubles(buffer);
  const std::vector<tk::spline *> splines{
      &bbh_bbe_pp7, &bbh_bbe_pp8, &bbh_bbe_pp13, &bbh_bbe_pp14, &bbh_bbe_ppbar2,
      &bbh_bbo_pp7, &bbh_bbo_pp8, &bbh_bbo_pp13, &bbh_bbo_pp14, &bbh_bbo_ppbar2,
      &ggh_bbe_pp7, &ggh_bbe_pp8, &ggh_bbe_pp13, &ggh_bbe_pp14, &ggh_bbe_ppbar2,
      &ggh_bbo_pp7, &ggh_bbo_pp8, &ggh_bbo_pp13, &ggh_bbo_pp14, &ggh_bbo_ppbar2,
      &ggh_tte_pp7, &ggh_tte_pp8, &ggh_tte_pp13, &ggh_tte_pp14, &ggh_tte_ppbar2,
      &ggh_tto_pp7, &ggh_tto_pp8, &ggh_tto_pp13, &ggh_tto_pp14, &ggh_tto_ppbar2,
      &ggh_tbe_pp7, &ggh_tbe_pp8, &ggh_tbe_pp13, &ggh_tbe_pp14, &ggh_tbe_ppbar2,
      &ggh_tbo_pp7, &ggh_tbo_pp8, &ggh_tbo_pp13, &ggh_tbo_pp14, &ggh_tbo_ppbar2,
  };
  for (auto spl : splines) {
    if (std::getline(data, buffer)) {
      spl->set_points(m, Utilities::ParseToDoubles(buffer));
    } else {
      throw(
          std::ios_base::failure("Could not read SusHi data from " + filepath));
    }
  }
}

double ScannerS::Tools::SushiTables::GG(double m, double cte, double cbe,
                                        double cto, double cbo,
                                        Collider coll) const {
  return GGe(m, cte, cbe, coll) + GGo(m, cto, cbo, coll);
}

double ScannerS::Tools::SushiTables::GGe(double m, double ct, double cb,
                                         Collider coll) const {
  switch (coll) {
  case Collider::LHC7:
    return ggh_tte_pp7(m) * ct * ct + ggh_tbe_pp7(m) * ct * cb +
           ggh_bbe_pp7(m) * cb * cb;
  case Collider::LHC8:
    return ggh_tte_pp8(m) * ct * ct + ggh_tbe_pp8(m) * ct * cb +
           ggh_bbe_pp8(m) * cb * cb;
  case Collider::LHC13:
    return ggh_tte_pp13(m) * ct * ct + ggh_tbe_pp13(m) * ct * cb +
           ggh_bbe_pp13(m) * cb * cb;
  case Collider::LHC14:
    return ggh_tte_pp14(m) * ct * ct + ggh_tbe_pp14(m) * ct * cb +
           ggh_bbe_pp14(m) * cb * cb;
  case Collider::TEV:
    return ggh_tte_ppbar2(m) * ct * ct + ggh_tbe_ppbar2(m) * ct * cb +
           ggh_bbe_ppbar2(m) * cb * cb;
  }
  return 0;
}

double ScannerS::Tools::SushiTables::GGo(double m, double ct, double cb,
                                         Collider coll) const {
  switch (coll) {
  case Collider::LHC7:
    return ggh_tto_pp7(m) * ct * ct + ggh_tbo_pp7(m) * ct * cb +
           ggh_bbo_pp7(m) * cb * cb;
  case Collider::LHC8:
    return ggh_tto_pp8(m) * ct * ct + ggh_tbo_pp8(m) * ct * cb +
           ggh_bbo_pp8(m) * cb * cb;
  case Collider::LHC13:
    return ggh_tto_pp13(m) * ct * ct + ggh_tbo_pp13(m) * ct * cb +
           ggh_bbo_pp13(m) * cb * cb;
  case Collider::LHC14:
    return ggh_tto_pp14(m) * ct * ct + ggh_tbo_pp14(m) * ct * cb +
           ggh_bbo_pp14(m) * cb * cb;
  case Collider::TEV:
    return ggh_tto_ppbar2(m) * ct * ct + ggh_tbo_ppbar2(m) * ct * cb +
           ggh_bbo_ppbar2(m) * cb * cb;
  }
  return 0;
}

double ScannerS::Tools::SushiTables::BB(double m, double cbe, double cbo,
                                        Collider coll) const {
  return BBe(m, cbe, coll) + BBo(m, cbo, coll);
}

double ScannerS::Tools::SushiTables::BBe(double m, double cb,
                                         Collider coll) const {
  switch (coll) {
  case Collider::LHC7:
    return bbh_bbe_pp7(m) * cb * cb;
  case Collider::LHC8:
    return bbh_bbe_pp8(m) * cb * cb;
  case Collider::LHC13:
    return bbh_bbe_pp13(m) * cb * cb;
  case Collider::LHC14:
    return bbh_bbe_pp14(m) * cb * cb;
  case Collider::TEV:
    return bbh_bbe_ppbar2(m) * cb * cb;
  }
  return 0;
}

double ScannerS::Tools::SushiTables::BBo(double m, double cb,
                                         Collider coll) const {
  switch (coll) {
  case Collider::LHC7:
    return bbh_bbo_pp7(m) * cb * cb;
  case Collider::LHC8:
    return bbh_bbo_pp8(m) * cb * cb;
  case Collider::LHC13:
    return bbh_bbo_pp13(m) * cb * cb;
  case Collider::LHC14:
    return bbh_bbo_pp14(m) * cb * cb;
  case Collider::TEV:
    return bbh_bbo_ppbar2(m) * cb * cb;
  }
  return 0;
}
